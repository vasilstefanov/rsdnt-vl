package com.example.residentevil.controllers;

import com.example.residentevil.entities.Virus;
import com.example.residentevil.services.CapitalService;
import com.example.residentevil.services.VirusService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/viruses")
public class VirusesController {
    private final VirusService virusService;
    private final CapitalService capitalService;

    public VirusesController(VirusService virusService, CapitalService capitalService) {
        this.virusService = virusService;
        this.capitalService = capitalService;
    }

    @GetMapping("/show")
    public String showAll(Model model, @PageableDefault() Pageable pageable) {
        Page<Virus> virusesPage = virusService.getAllViruses(pageable);

        int pageItems = 10;
        int previousPageNumber = pageable.previousOrFirst().getPageNumber();
        int nextPageNumber = virusesPage.getNumber();
        if (virusesPage.hasNext()) {
            nextPageNumber = virusesPage.nextPageable().getPageNumber();
        }

        int startPageItem = (virusesPage.getNumber() / pageItems) * pageItems;
        List<Integer> pageNumbers = new ArrayList<>();
        for (int i = 0; i < pageItems; i++) {
            pageNumbers.add(startPageItem + i);
        }

        model.addAttribute("virusesPage", virusesPage);
        model.addAttribute("previousPageNumber", previousPageNumber);
        model.addAttribute("pageNumbers", pageNumbers);
        model.addAttribute("nextPageNumber", nextPageNumber);

        return "viruses/all-viruses";
    }

    @GetMapping("/add")
    public String add(Model model, @ModelAttribute Virus virus) {
        model.addAttribute("capitals", capitalService.getAllCapitals());
        return "viruses/save-virus";
    }

    @PostMapping("/add")
    public String postAdd(Model model, @Valid @ModelAttribute Virus virus, BindingResult bindingResult) {
        virusService.validate(virus, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("capitals", capitalService.getAllCapitals());
            return "viruses/save-virus";
        }

        virusService.save(virus);

        return "redirect:show";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable String id, Model model) {
        model.addAttribute("capitals", capitalService.getAllCapitals());

        Virus virusToEdit = virusService.get(id);
        model.addAttribute("virus", virusToEdit);

        return "viruses/save-virus";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable String id) {
        try {
            virusService.delete(id);
        } catch (Exception e) {
            throw new VirusException(String.format("Could not delete virus with id '%s'.", id), e);
        }
        return "redirect:../show";
    }

    @ExceptionHandler
    public String virusExceptionHandler(Exception exception, Exception e, Model model) {
        model.addAttribute("message", exception.getMessage());
        model.addAttribute("error", exception.getCause().getMessage());
        return "/error/virus-error";
    }
}