package com.example.residentevil.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Virus exception.")
class VirusException extends RuntimeException {
    VirusException(String message, Throwable cause) {
        super(message, cause);
    }
}
