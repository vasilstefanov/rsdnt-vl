package com.example.residentevil.services;

import com.example.residentevil.entities.Virus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;

import java.util.List;

public interface VirusService {
    @SuppressWarnings("unused")
    List<Virus> getAllViruses();

    Virus get(String id);

    void save(Virus virus);

    void delete(String id);

    Page<Virus> getAllViruses(Pageable pageable);

    void validate(Virus virus, BindingResult bindingResult);
}
