package com.example.residentevil.services;

import com.example.residentevil.entities.Virus;
import com.example.residentevil.repos.VirusRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Objects;

@Service
public class VirusServiceImpl implements VirusService {

    private VirusRepository virusRepository;

    public VirusServiceImpl(VirusRepository virusRepository) {
        this.virusRepository = virusRepository;
    }

    @Override
    public List<Virus> getAllViruses() {
        return virusRepository.findAll();
    }

    @Override
    public Virus get(String id) {
        return virusRepository.getOne(id);
    }

    @Override
    public void save(Virus virus) {
        virusRepository.save(virus);
    }

    @Override
    public void delete(String id) {
        virusRepository.deleteById(id);
    }

    @Override
    public Page<Virus> getAllViruses(Pageable pageable) {
        return virusRepository.findAllByOrderByName(pageable);
    }

    @Override
    public void validate(Virus virus, BindingResult bindingResult) {
        Virus existingVirus = virusRepository.findByName(virus.getName());
        if (existingVirus == null) {
            return;
        }

        if (Objects.equals(existingVirus.getId(), virus.getId())) {
            return;
        }

        bindingResult.rejectValue("name", "error.virus.name",
                String.format("Virus with name '%s' already exists.", existingVirus.getName()));

    }
}