package com.example.residentevil.services;

import com.example.residentevil.entities.Capital;

import java.util.List;

public interface CapitalService {
    List<Capital> getAllCapitals();
}
