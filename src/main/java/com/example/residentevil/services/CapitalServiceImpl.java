package com.example.residentevil.services;

import com.example.residentevil.entities.Capital;
import com.example.residentevil.repos.CapitalRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CapitalServiceImpl implements CapitalService {

    private CapitalRepository capitalRepository;

    public CapitalServiceImpl(CapitalRepository capitalRepository) {
        this.capitalRepository = capitalRepository;
    }

    @Override
    public List<Capital> getAllCapitals() {
        return capitalRepository.findAll();
    }
}
