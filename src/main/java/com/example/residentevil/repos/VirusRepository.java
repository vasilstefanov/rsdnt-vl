package com.example.residentevil.repos;

import com.example.residentevil.entities.Virus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VirusRepository extends JpaRepository<Virus, String> {
    Page<Virus> findAllByOrderByName(Pageable pageable);

    Virus findByName(String name);
}
