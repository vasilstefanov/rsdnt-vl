package com.example.residentevil.security.entities;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.Collection;

@Entity
@Table(name = "roles")
public class Role implements GrantedAuthority {
    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    @Pattern(regexp = "(ROLE_USER|ROLE_MODERATOR|ROLE_ADMIN)")
    private String authority;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "roles_rights",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "role_right_id"))
    private Collection<Role> roleRights;

    public Role() {
    }

    public Role(String authority) {
        this.authority = authority;
    }

    public Collection<Role> getRoleRights() {
        return roleRights;
    }

    public void setRoleRights(Collection<Role> roleRights) {
        this.roleRights = roleRights;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String toString() {
        return authority;
    }
}