package com.example.residentevil.security.controllers;

import com.example.residentevil.security.models.LoginModel;
import com.example.residentevil.security.models.UserEditModel;
import com.example.residentevil.security.models.UserRegistrationModel;
import com.example.residentevil.security.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UsersController {
    private UserService userService;

    public UsersController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String register(@ModelAttribute("user") UserRegistrationModel userRegistrationModel) {
        return ViewName.SAVE_USER;
    }

    @PostMapping("/register")
    public String postRegister(@Valid @ModelAttribute("user") UserRegistrationModel userRegistrationModel,
                               BindingResult bindingResult) {
        userService.validateRegistration(userRegistrationModel, bindingResult);
        if (bindingResult.hasErrors()) {
            return ViewName.SAVE_USER;
        }
        userService.registerUser(userRegistrationModel);
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String login(@ModelAttribute("login") LoginModel loginModel) {
        loginModel.setRememberMe(true);
        return ViewName.LOGIN;
    }

    @GetMapping("/users/error")
    public String error() {
        return ViewName.ERROR;
    }

    @GetMapping("/users")
    public String allUsers(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        return ViewName.ALL_USERS;
    }

    @GetMapping("/users/edit/{id}")
    public String edit(@PathVariable Long id, Model model) {
        model.addAttribute("user", userService.getUserEditModel(id));
        return ViewName.EDIT_USER;
    }

    @PostMapping("/users/edit/{id}")
    public String postEdit(@Valid @ModelAttribute("user") UserEditModel user, BindingResult bindingResult,
                           @PathVariable Long id, Model model) {
        if (bindingResult.hasErrors()) {
            return ViewName.EDIT_USER;
        }

        userService.updateUserDetails(user);

        return "redirect:/users";
    }

    @GetMapping("/users/delete/{id}")
    public String delete(@PathVariable Long id) {
        userService.delete(id);
        return "redirect:..";
    }

    private static class ViewName {
        static final String SAVE_USER = "users/save-user";
        static final String LOGIN = "users/login";
        static final String ALL_USERS = "users/all-users";
        static final String EDIT_USER = "users/edit-user";
        static final String ERROR = "users/error";
    }
}