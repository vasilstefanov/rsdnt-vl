package com.example.residentevil.security.config;

import com.example.residentevil.security.entities.Role;
import com.example.residentevil.security.entities.User;
import com.example.residentevil.security.models.UserRegistrationModel;
import com.example.residentevil.security.repos.RoleRepository;
import com.example.residentevil.security.services.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
public class DatabaseInitConfig {
    private final RoleRepository roleRepository;

    private final UserService userService;

    public DatabaseInitConfig(RoleRepository roleRepository, UserService userService) {
        this.roleRepository = roleRepository;
        this.userService = userService;
    }

    @Bean
    public CommandLineRunner populateDatabaseData() {
        return args -> {
            List<Role> roles = roleRepository.findAll();
            if (roles.isEmpty()) {
                Role user = new Role("ROLE_USER");
                roleRepository.save(user);

                Role moderator = new Role("ROLE_MODERATOR");
                moderator.setRoleRights(Collections.singletonList(user));
                roleRepository.save(moderator);

                Role admin = new Role("ROLE_ADMIN");
                admin.setRoleRights(Arrays.asList(moderator, user));
                roleRepository.save(admin);
            }

            List<User> users = userService.getAllUsers();
            if (users.isEmpty()) {
                UserRegistrationModel adminRegistration = new UserRegistrationModel();
                adminRegistration.setUsername("admin");
                adminRegistration.setPassword("a");
                adminRegistration.setEmail("admin@example.com");
                userService.registerUser(adminRegistration, true);
            }
        };
    }
}