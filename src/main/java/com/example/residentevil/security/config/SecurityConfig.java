package com.example.residentevil.security.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {
    private UserDetailsService userDetailsService;

    public SecurityConfig(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers("/css/**", "/js/**", "/images/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
                .disable();

        http.authorizeRequests()
                .antMatchers("/login", "/register", "/")
                .permitAll() // Anonymous (not logged-in) clients

                .antMatchers("/viruses/show", "/logout") // [/] is already authorized for all
                .hasRole("USER")

                .antMatchers("/viruses/add", "/viruses/edit/*", "/viruses/delete/*")
                .hasRole("MODERATOR")

                .antMatchers("/**") // Rest of the paths are authorized for ADMIN only
                .hasRole("ADMIN");

        http.formLogin()
                .loginPage("/login")
                .permitAll();

        http.logout()
                .logoutUrl("/logout")
                .permitAll();

        http.rememberMe()
                .userDetailsService(userDetailsService)
                .rememberMeParameter("rememberMe")
                .tokenValiditySeconds(10 * 60);

        http.exceptionHandling()
                .accessDeniedPage("/unauthorized");
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/unauthorized").setViewName("security/unauthorized");
    }
}