package com.example.residentevil.security.services;

import com.example.residentevil.security.entities.User;
import com.example.residentevil.security.models.UserEditModel;
import com.example.residentevil.security.models.UserRegistrationModel;
import org.springframework.validation.BindingResult;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

public interface UserService {
    List<User> getAllUsers();

    User registerUser(@Valid UserRegistrationModel user);

    void delete(Long id);

    default void validateRegistration(UserRegistrationModel registerUserModel, BindingResult bindingResult) {
        if (!Objects.equals(registerUserModel.getConfirmPassword(), registerUserModel.getPassword())) {
            bindingResult.rejectValue("confirmPassword", "error.user.confirmPassword",
                    "Confirm Password should match the Password.");
        }
    }

    UserEditModel getUserEditModel(Long id);

    User updateUserDetails(@Valid UserEditModel user);

    User registerUser(UserRegistrationModel adminRegistration, boolean isAdmin);
}
