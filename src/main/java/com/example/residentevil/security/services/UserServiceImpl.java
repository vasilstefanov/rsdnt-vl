package com.example.residentevil.security.services;

import com.example.residentevil.security.entities.Role;
import com.example.residentevil.security.entities.User;
import com.example.residentevil.security.models.UserEditModel;
import com.example.residentevil.security.models.UserRegistrationModel;
import com.example.residentevil.security.repos.RoleRepository;
import com.example.residentevil.security.repos.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import javax.validation.Valid;
import java.util.List;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {
    private final BCryptPasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final ModelMapper modelMapper;

    public UserServiceImpl(BCryptPasswordEncoder passwordEncoder, UserRepository userRepository,
                           RoleRepository roleRepository, ModelMapper modelMapper) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User registerUser(@Valid UserRegistrationModel userRegistrationModel) {
        return registerUser(userRegistrationModel, false);
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public void validateRegistration(UserRegistrationModel registerUserModel, BindingResult bindingResult) {
        UserService.super.validateRegistration(registerUserModel, bindingResult);

        User existingUser = userRepository.findByUsername(registerUserModel.getUsername());
        if (existingUser == null) {
            return;
        }

        bindingResult.rejectValue("username", "error.user.username",
                String.format("User with username '%s' already exists.", existingUser.getUsername()));
    }

    @Override
    public UserEditModel getUserEditModel(Long id) {
        User user = userRepository.getOne(id);
        UserEditModel userEditModel = modelMapper.map(user, UserEditModel.class);
        userEditModel.setPassword("");
        return userEditModel;
    }

    @Override
    public User updateUserDetails(@Valid UserEditModel userEditModel) {
        User user = userRepository.getOne(userEditModel.getId());
        String passwordBackup = user.getPassword();

        modelMapper.map(userEditModel, user);

        if (userEditModel.getPassword().isEmpty()) {
            user.setPassword(passwordBackup);
        } else {
            user.setPassword(passwordEncoder.encode(userEditModel.getPassword()));
        }

        user.setRole(roleRepository.findByAuthority(userEditModel.getRole()));

        return userRepository.save(user);
    }

    @Override
    public User registerUser(UserRegistrationModel userRegistrationModel, boolean isAdmin) {
        User user = new User();

        user.setUsername(userRegistrationModel.getUsername());
        user.setPassword(passwordEncoder.encode(userRegistrationModel.getPassword()));
        user.setEmail(userRegistrationModel.getEmail());

        Role role;
        if (isAdmin) {
            role = roleRepository.findByAuthority("ROLE_ADMIN");
        } else {
            role = roleRepository.findByAuthority("ROLE_USER");
        }
        if (role != null) {
            user.setRole(role);
        }

        return userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        return user;
    }
}