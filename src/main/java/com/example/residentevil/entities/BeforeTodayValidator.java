package com.example.residentevil.entities;

import org.springframework.lang.NonNull;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class BeforeTodayValidator implements ConstraintValidator<BeforeToday, LocalDate> {

    private static boolean isBeforeToday(@NonNull LocalDate date) {
        LocalDate today = LocalDate.now();
        return date.isBefore(today);
    }

    @Override
    public boolean isValid(LocalDate date, ConstraintValidatorContext context) {
        return date != null && isBeforeToday(date);
    }

}
