package com.example.residentevil.entities;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "viruses")
public class Virus {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @NotEmpty(message = "Cannot be empty.")
    @Size(min = 3, max = 10, message = "Should be between 3 and 10 symbols.")
    @Column(unique = true)
    private String name;

    @NotEmpty(message = "Cannot be empty.")
    @Column(columnDefinition = "text")
    @Size(min = 5, max = 100, message = "Should be between 5 and 100 symbols.")
    private String description;

    @Size(max = 50, message = "Should have a maximum of 50 symbols.")
    private String sideEffect;

    @Pattern(regexp = "^(Corp|corp\\.)$", message = "Should be either \"Corp\" or \"corp.\" without quotes.")
    private String creator;

    private Boolean isDeadly;

    private Boolean isCurable;

    @NotEmpty(message = "You must choose mutation.")
    @Pattern(regexp = "^(ZOMBIE|T_078_TYRANT|GIANT_SPIDER)$",
            message = "Should be either ZOMBIE or T_078_TYRANT or GIANT_SPIDER.")
    private String mutation;

    @Min(value = 0, message = "Should be minimum 0.")
    @Max(value = 100, message = "Should be maximum 100.")
    private Integer turnoverRate;

    @Min(value = 1, message = "Should be minimum 1.")
    @Max(value = 12, message = "Should be maximum 12.")
    private Integer hoursUntilTurnToMutation;

    @NotEmpty(message = "You must choose magnitude.")
    @Pattern(regexp = "^(Low|Medium|High)$", message = "Should be either 'Low' or 'Medium' or 'High'.")
    private String magnitude;

    @Column(updatable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @BeforeToday
    private LocalDate releasedOn;

    @NotEmpty(message = "You must select one or more capitals.")
    @ManyToMany
    @JoinTable(name = "capitals_viruses",
            joinColumns = @JoinColumn(name = "virus_id"),
            inverseJoinColumns = @JoinColumn(name = "capital_id"))
    private List<Capital> capitals = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSideEffect() {
        return sideEffect;
    }

    public void setSideEffect(String sideEffect) {
        this.sideEffect = sideEffect;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Boolean getDeadly() {
        return isDeadly;
    }

    public void setDeadly(Boolean deadly) {
        isDeadly = deadly;
    }

    public Boolean getCurable() {
        return isCurable;
    }

    public void setCurable(Boolean curable) {
        isCurable = curable;
    }

    public String getMutation() {
        return mutation;
    }

    public void setMutation(String mutation) {
        this.mutation = mutation;
    }

    public Integer getTurnoverRate() {
        return turnoverRate;
    }

    public void setTurnoverRate(Integer turnoverRate) {
        this.turnoverRate = turnoverRate;
    }

    public Integer getHoursUntilTurnToMutation() {
        return hoursUntilTurnToMutation;
    }

    public void setHoursUntilTurnToMutation(Integer hoursUntilTurnToMutation) {
        this.hoursUntilTurnToMutation = hoursUntilTurnToMutation;
    }

    public String getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(String magnitude) {
        this.magnitude = magnitude;
    }

    public LocalDate getReleasedOn() {
        return releasedOn;
    }

    public void setReleasedOn(LocalDate releasedOn) {
        this.releasedOn = releasedOn;
    }

    public List<Capital> getCapitals() {
        return capitals;
    }

    public void setCapitals(List<Capital> capitals) {
        this.capitals = capitals;
    }

    @Override
    public String toString() {
        return "Virus{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", sideEffect='" + sideEffect + '\'' +
                ", creator='" + creator + '\'' +
                ", isDeadly=" + isDeadly +
                ", isCurable=" + isCurable +
                ", mutation='" + mutation + '\'' +
                ", turnoverRate=" + turnoverRate +
                ", hoursUntilTurnToMutation=" + hoursUntilTurnToMutation +
                ", magnitude='" + magnitude + '\'' +
                ", releasedOn=" + releasedOn +
                ", capitals=" + capitals +
                '}';
    }

}
